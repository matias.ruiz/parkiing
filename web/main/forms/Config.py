from flask_wtf import FlaskForm
import wtforms as wtf


class ConfigUser(FlaskForm):

    name = wtf.StringField(
        label="name",
        validators=[wtf.validators.DataRequired(message="Este campo es obligatorio")],
        render_kw={"placeholder": "Nombre"}
    )

    submit_button = wtf.SubmitField(label="Guardar")

